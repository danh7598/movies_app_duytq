import { AppRegistry } from 'react-native';
import App from './app/src/App';
import Login from './app/src/components/screen/login/Login'
import Walkthrough from './app/src/components/screen/walkthrough/Walkthrough_Screen'
import Movie from './app/src/components/screen/main/movie/Movie'
import AppProvider from './app/src/appProvider'
import MovieItem from './app/src/components/screen/main/movie/Movie_Detail'
AppRegistry.registerComponent('movie_app_duytq', () => AppProvider);
