import {Platform} from 'react-native'

export const SERVER_ADDRESS = 'https://api.themoviedb.org/3/';
const API_KEY = "d7140789fae0417a79a2b7aabfb84c8f";

// const getParam = (method: string, data: any, token = null) => {
//
//     return {
//         method: method,
//         headers: {
//             'Accept': 'application/json',
//             'Content-Type': 'application/json',
//             'x-access-token': token
//         },
//         body: JSON.stringify(data)
//     }
// };
//
// export const request = async (endpoint: string, method: string, body: any) => {
//     let token = await getToken();
//     // let connect = await isConnect();
//     // if (connect) {
//     return fetch(
//         SERVER_ADDRESS,
//         getParam(method, body, token)
//     )
//         .then(res => {
//             try {
//                 return res.json()
//             } catch (e) {
//                 throw e;
//             }
//         })
//         .then((data) => handleError(data))
//         .catch(error => {
//             throw error;
//         });
// };


export const request = async (endpoint: string, method: string, body: any) => {
    return fetch(SERVER_ADDRESS + endpoint,)
        .then((response) => response.json())
        .then((responseJson) => {
            console.log('Api.js row 45'+ responseJson);
            return responseJson;
        }).catch(error => {
            console.error('Api.js row 48' + error);
        });
};


export const getMovieList = (pageNumber) => {
    return request('movie/' + 'upcoming' + '?api_key=' + API_KEY + '&page=' + pageNumber, 'GET')
};
export const getTVList = (pageNumber) => {
    return request('tv/top_rated' + '?api_key=' + API_KEY + '&page=' + pageNumber, 'GET')
};
export const getSearchList = (query,pageNumber) => {
    return request('search/movie' + '?api_key=' + API_KEY + '&query=' + query + '&page=' + pageNumber , 'GET')
};
export const getSearchTVList = (query,pageNumber) => {
    return request('search/tv' + '?api_key=' + API_KEY + '&query=' + query + '&page=' + pageNumber , 'GET')
};
