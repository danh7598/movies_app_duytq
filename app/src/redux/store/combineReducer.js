import {combineReducers, createStore, applyMiddleware} from "redux";
import thunk from 'redux-thunk';
import {navigationReducer} from "../router/NavigationReducer";
import {movieReducer} from "../movie/MovieReducer";
import {tvReducer} from "../tv/TvReducer";

const reducer = combineReducers({
    nav: navigationReducer,
    movieState: movieReducer,
    tvState: tvReducer

});

export const store = createStore(reducer, applyMiddleware(thunk));