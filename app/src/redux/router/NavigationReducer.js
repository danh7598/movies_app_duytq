 import {AppRouter} from "./AppRouter";
import {NavigationActions} from "react-navigation";


export const navigationState = AppRouter.router.getStateForAction(NavigationActions.reset({
    index: 0,
    actions: [
        NavigationActions.navigate({
            routeName: 'Splash', //screen root ban đầu
        }),
    ],
}));

export const navigationReducer = (state = navigationState, action) => {
    if(state && state.index > 2 && action.routeName === 'PostProductFromCategory') {
        let routes = state.routes.slice(0, state.routes.length - action.params.page);
        return {
            index: routes.length - 1,
            routes: routes
        };
    }

    // return defaultGetStateForAction(action, state)

    const nextState = preventMultiTaps(action, state) ? null : AppRouter.router.getStateForAction(action, state);
    return nextState || state;
};

// const defaultGetStateForAction = (action, state) => {
//     const nextState = preventMultiTaps(action, state) ? null : AppRouter.router.getStateForAction(action, state);
//     return nextState || state;
// };

/**
 * check multi click
 *
 * @returns {boolean}
 */
const preventMultiTaps = (action, state) => {
    const {type, routeName, params} = action;
    return !!(
        state &&
        type === NavigationActions.NAVIGATE &&
        routeName === state.routes[state.routes.length - 1].routeName
        && JSON.stringify(params) === JSON.stringify(state.routes[state.routes.length - 1].params)
    )
};
