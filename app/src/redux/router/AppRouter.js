import React from 'react';
import {addNavigationHelpers, StackNavigator} from "react-navigation";
import {connect} from "react-redux";
import PropTypes from 'prop-types';
import SplashComponent from '../../components/screen/splash/SplashComponent';
import Walkthrough from '../../components/screen/walkthrough/Walkthrough_Screen';
import Login from '../../components/screen/login/Login';
import MainTabRouter from '../MainRouter';
import PopularTVList from "../../components/screen/main/tv/PopularTVList";
import ProfileSetting from "../../components/screen/main/profile/ProfileSetting";
import MovieItem from '../../components/screen/main/movie/Movie_Detail';
import PopularMovieList from "../../components/screen/main/movie/PopularMovieList";
import TVDetail from "../../components/screen/main/tv/TVDetail";
import Search from '../../components/screen/search/Search';
import CommentScreen from '../../components/screen/comment/commentScreen';
import About from '../../components/screen/main/profile/About';
export const AppRouter = StackNavigator({
    Splash: {
        screen: SplashComponent
    },
    WalkThrough: {
        screen: Walkthrough
    },
    Login:{
        screen: Login
    },
    Main:{
        screen: MainTabRouter
    },
    PopularTVList:{
        screen: PopularTVList
    },
    PopularMovieList:{
        screen: PopularMovieList
    },
    ProfileSetting:{
        screen: ProfileSetting
    },
    MovieItem:{
        screen: MovieItem
    },
    TVDetail:{
        screen: TVDetail
    },
    Search:{
        screen: Search
    },
    Comment:{
        screen: CommentScreen
    },
    About:{
        screen: About
    }
},{
    headerMode: 'none'
});

const AppWithNavigationState = ({dispatch, nav}) => (
    <AppRouter
        navigation={addNavigationHelpers({dispatch, state: nav})}
    />
);

AppWithNavigationState.propTypes = {
    dispatch: PropTypes.func.isRequired,
    nav: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
    nav: state.nav,
});

export default connect(mapStateToProps, null)(AppWithNavigationState);