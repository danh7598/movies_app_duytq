// import React from 'react';
// import {
//     View, StyleSheet, Text,
//     TouchableWithoutFeedback, ScrollView,
//     ImageBackground,
//     Animated
// } from 'react-native';
// import {sizeFont, sizeWidth} from "../utils/Size";
// import {connect} from "react-redux";
// import {navigateToPage} from "./router/NavigationAction";
// // import {checkLogin} from "../redux/login/LoginAction";
// // import {BOTTOM_BAR_IOS_HEIGHT} from "../Constant";
// // import {APP_COLOR, SIZE_FONT_SMALL} from "../../res/style/AppStyle";
// // import EventRegister, {REAL_TIME, SCROLL_TO_TOP} from "../utils/EventRegister";
//
// class TabBarComponent extends React.Component {
//
//     render() {
//         const {
//             navigation,
//             renderIcon,
//             activeTintColor,
//             inactiveTintColor,
//             jumpToIndex,
//             sumNotify
//         } = this.props;
//         const {routes} = navigation.state;
//         return (
//             <ImageBackground style={[styles.TabBar]}
//                              resizeMode={'cover'}
//                              source={require('../../res/images/bg_tab_bar.png')}>
//                 {routes && routes.map((route, index) => {
//                     const focused = index === navigation.state.index;
//                     const tintColor = focused ? activeTintColor : inactiveTintColor;
//                     const scene = {route, index, focused};
//                     const label = this.props.getLabel({...scene, tintColor});
//                     return (
//                         <TouchableWithoutFeedback
//                             key={route.key}
//                             style={styles.Tab}
//                             onPress={() => {
//                                 index !== 2 ? this.customJumpToIndex(jumpToIndex, index) : this.navigate()
//                             }}
//                         >
//                             <View style={{flex: 1, alignItems: 'center'}}>
//                                 <View style={styles.Tab}>
//                                     {renderIcon({
//                                         route,
//                                         index,
//                                         focused,
//                                         tintColor
//                                     })}
//                                     {index === 3 && sumNotify > 0 && this.renderNotifyNumber(sumNotify)}
//                                 </View>
//                                 <Text style={{
//                                     fontSize: SIZE_FONT_SMALL,
//                                     marginBottom: sizeWidth(1),
//                                     color: tintColor,
//                                     fontWeight: 'bold'
//                                 }}>{label}</Text>
//                                 <View style={{height: BOTTOM_BAR_IOS_HEIGHT - 10, width: '100%'}}/>
//                             </View>
//                         </TouchableWithoutFeedback>
//                     );
//                 })}
//             </ImageBackground>
//         );
//     }
//
//     renderNotifyNumber = (num) => {
//         num = num > 5 ? '5+' : num.toString();
//         return (
//             <View style={styles.numberContainer}>
//                 <Text style={{color: 'white', fontSize: sizeFont(3), fontWeight: 'bold'}}>{num}</Text>
//             </View>
//         );
//     };
//
//     customJumpToIndex = (jumpToIndex, index) => {
//         jumpToIndex(index);
//         EventRegister.emit(SCROLL_TO_TOP)
//     };
//
//     navigate = () => {
//         this.props.checkLogin(() => this.props.navigateToPage('PostProduct', {product: null, isAdd: true}));
//     }
// }
//
// const styles = StyleSheet.create({
//     TabBar: {
//         height: sizeWidth(21) + BOTTOM_BAR_IOS_HEIGHT,
//         width: '100%',
//         flexDirection: 'row'
//     },
//     Tab: {
//         alignSelf: 'stretch',
//         flex: 1,
//         alignItems: 'center',
//         justifyContent: 'center',
//         paddingTop: sizeWidth(2),
//     },
//     numberContainer: {
//         height: sizeWidth(4.4),
//         width: sizeWidth(4.4),
//         borderRadius: sizeWidth(2.2),
//         alignItems: 'center',
//         position: 'absolute',
//         top: sizeWidth(3.5),
//         right: sizeWidth(4),
//         backgroundColor: 'red',
//     }
// });
//
// export default connect(
//     state => ({
//         sumNotify: state.loadingState.sumNotify
//     }),
//     {navigateToPage, checkLogin}
// )(TabBarComponent);