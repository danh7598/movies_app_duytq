import React, {Component} from 'react';
import {
    Text,
    View,
    StyleSheet, Image,
} from 'react-native';
import PropTypes from 'prop-types';
import {sizeWidth} from "../utils/Size";

export default class BottomTabItem extends Component {
    render() {
        const {icon, active, isCamera} = this.props;
        let style = active ? styles.IconActive : styles.Icon;
        if (isCamera) {
            return (
                <Image resizeMode={'contain'} style={styles.CameraActive} source={icon}/>
            )
        }
        return (
            <Image resizeMode={'contain'} style={style} source={icon}/>
        );
    }
}

BottomTabItem.propTypes = {
    icon: PropTypes.any,
};

const styles = StyleSheet.create({
    Icon: {
        width: sizeWidth(6),
        height: sizeWidth(6),
    },
    IconActive: {
        width: sizeWidth(6),
        height: sizeWidth(6),
    },
    CameraActive: {
        width: sizeWidth(13),
        height: sizeWidth(13),
        marginTop: sizeWidth(-1),
    }
});
