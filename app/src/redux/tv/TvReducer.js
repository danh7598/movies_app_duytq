import {
    ACTION_GET_MORE_TOP_RATE_TV,
    ACTION_GET_TOP_RATE_TV, ACTION_HIDE_LOADING, ACTION_REFRESH_UPCOMING_TV,
    ACTION_RESET_SEARCH_TV,
    ACTION_SEARCH_TV, ACTION_SHOW_LOADING
} from "../Action";

export const tvState = {
    topRateTv: [],
    extraTopRateTv: [],
    listSearchTV:[],
    showLoading: true
};
export const tvReducer = (state = tvState, action) => {
    switch (action.type) {
        case ACTION_GET_MORE_TOP_RATE_TV:
        return {
            ...state,
            extraTopRateTv: [...state.extraTopRateTv,...action.payload.results]
        };
        case ACTION_GET_TOP_RATE_TV:
            return {
                ...state,
                topRateTv: [...action.payload.results],
            };
        case ACTION_REFRESH_UPCOMING_TV:
            return {
                ...state,
                topRateTv: [],
                extraTopRateTv: [],
            };
        case ACTION_SEARCH_TV:
            return {
                ...state,
                listSearchTV: [...state.listSearchTV,...action.payload.results]
            };
        case ACTION_RESET_SEARCH_TV:
            console.log("RESET SEARCH LIST");
            return {
                ...state,
                listSearchTV: []
            };
        case ACTION_SHOW_LOADING:
            console.log("SHOW LOADING");
            return {
                ...state,
                showLoading: true
            };
        case ACTION_HIDE_LOADING:
            console.log("HIDE LOADING");
            return {
                ...state,
                showLoading: false
            };
        default:
            return state;
    }
};
