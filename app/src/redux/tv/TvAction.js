import {} from "../Action";
import {ACTION_GET_TOP_RATE_TV} from "../Action";
import {getSearchTVList, getTVList} from "../../../api/Api";
import {ACTION_SEARCH_MOVIE} from "../Action";
import {ACTION_RESET_SEARCH_TV} from "../Action";
import {ACTION_HIDE_LOADING} from "../Action";
import {ACTION_SHOW_LOADING} from "../Action";
import {ACTION_GET_MORE_TOP_RATE_TV} from "../Action";
import {ACTION_REFRESH_UPCOMING_TV} from "../Action";

export const actionGetTVList = (pageNumber) => {
    return dispatch => {
        dispatch(showLoading());
        getTVList(pageNumber)
            .then((data) => {
                dispatch(getTopRateTVList(data.results));
                dispatch(hideLoading())
            })
            .catch(error => console.log('TVAction.js row 15' + 'Error load TV List'))
    }
};
export const actionGetMoreTVList = (pageNumber) => {
    return dispatch => {
        dispatch(showLoading());
        getTVList(pageNumber)
            .then((data) => {
                dispatch(getMoreTVList(data.results));
                dispatch(hideLoading())
            })
            .catch(error => console.log('TVAction.js row 27' + 'Error load TV List'))
    }
};
export const actionRefreshTVList = () => {
    return dispatch => {
        dispatch(refreshTVList());
    }
};
export const actionSearchTV = (query,pageNumber) => {
    return dispatch => {
        dispatch(showLoading());
        getSearchTVList(query,pageNumber)
            .then((data) => {
                dispatch(searchList(data.results));
                dispatch(hideLoading())
            })
            .catch(error => console.log('TVAction.js row 27' + 'Error load TV List'))
    }
};
export const actionResetSearchTV = () => {
    return dispatch => {
        dispatch(resetSearchList());
    }
};

const getMoreTVList = (results) => {
    return {
        type: ACTION_GET_MORE_TOP_RATE_TV,
        payload: {
            results
        }
    }
};
const getTopRateTVList = (results) => {
    return {
        type: ACTION_GET_TOP_RATE_TV,
        payload: {
            results
        }
    }
};
const searchList = (results) => {
    return {
        type: ACTION_SEARCH_MOVIE,
        payload: {
            results
        }
    }
};
const resetSearchList = () => {
    return {
        type: ACTION_RESET_SEARCH_TV,
    }
};
const showLoading = () => {
    return {
        type: ACTION_SHOW_LOADING,
    }
};
const hideLoading = () => {
    return {
        type: ACTION_HIDE_LOADING
    }
};
const refreshTVList = () => {
    return {
        type: ACTION_REFRESH_UPCOMING_TV
    }
};
