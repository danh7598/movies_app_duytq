import {TabNavigator,TabBarBottom} from "react-navigation";
import React from "react";
import BottomTabItem from "./BottomTabItem";
import {sizeFont, sizeWidth} from "../utils/Size";
import TabBarComponent from "./TabBarComponent";
import Movie from '../components/screen/main/movie/Movie';
import Tv from '../components/screen/main/tv/Tv';
import Profile from '../components/screen/main/profile/Profile';
export default MainTabRouter = TabNavigator({
    Movie: {
        screen: Movie,
        navigationOptions: {
            title: 'MOVIE',
            tabBarIcon: ({focused}) => (
                focused
                    ? <BottomTabItem icon={require('../../resources/icon/ic_tab_movie_active.png')}
                                     active={true}/>
                    : <BottomTabItem icon={require('../../resources/icon/ic_tab_movie.png')}
                                     active={false}/>
            ),
        }
    },
    Television: {
        screen: Tv,
        navigationOptions: {
            title: 'TV',
            tabBarIcon: ({focused}) => (
                focused
                    ? <BottomTabItem icon={require('../../resources/icon/icon_tv_active.png')}
                                     active={true}/>
                    : <BottomTabItem icon={require('../../resources/icon/ic-tv-nonactive.png')}
                                     active={false}/>
            ),
        }
    },
    Profile: {
        screen: Profile,
        navigationOptions: {
            title: 'PROFILE',
            tabBarIcon: ({focused}) => (
                focused
                    ? <BottomTabItem icon={require('../../resources/icon/ic_tab_profile_active.png')}
                                     active={true}/>
                    : <BottomTabItem icon={require('../../resources/icon/ic_tab_profile.png')}
                                     active={false}/>
            ),
        }
    }
}, {
    tabBarComponent:TabBarBottom,
    tabBarPosition: 'bottom',
    initialRouteName: 'Movie',
    backBehavior: 'none',
    swipeEnabled: false,
    animationEnabled: true,
    tabBarOptions: {
        activeBackgroundColor:'#FFFFFF',
        inactiveBackgroundColor:'#FFFFFF',
        inactiveTintColor: '#8F8F8F',
        activeTintColor:'#D14204',
        labelStyle:{
            fontSize:sizeFont(3),
            marginBottom:sizeWidth(1)
        },
    }
});