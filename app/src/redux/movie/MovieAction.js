import {
    ACTION_GET_UPCOMING_MOVIE, ACTION_GET_MORE_UPCOMING_MOVIE, ACTION_SEARCH_MOVIE,
    ACTION_RESET_SEARCH_MOVIE, ACTION_SHOW_LOADING, ACTION_HIDE_LOADING, ACTION_REFRESH_UPCOMING_MOVIE
} from "../Action";
import {getMovieList, getSearchList} from "../../../api/Api";

export const actionGetMovieList = (pageNumber) => {
    return dispatch => {
        dispatch(showLoading());
        getMovieList(pageNumber)
            .then((data) => {
                //console.log(JSON.stringify(data));
                dispatch(showMovieList(data.results));
                dispatch(hideLoading());
            })
            .catch(error => showError(error))
    }
};
export const actionGetMoreMovieList = (pageNumber) => {
    return dispatch => {
        console.log('ACTION GET MORE MOVIE');
        dispatch(showLoading());
        getMovieList(pageNumber)
            .then((data) => {
                //console.log(JSON.stringify(data));
                dispatch(getMoreMovieList(data.results));
                dispatch(hideLoading());

            })
            .catch(error => showError(error))
    }
};
export const actionRefreshMovieList = () => {
    return dispatch => {
        dispatch(refreshMovieList());
    }
};
export const actionSearchMovie = (query,pageNumber) => {
    return dispatch => {
        console.log('ACTION SEARCH MOVIE');
        dispatch(showLoading());
        getSearchList(query,pageNumber)
            .then((data) => {
                console.log("SEARCH TEST" + JSON.stringify(data));
                dispatch(searchList(data.results));
                dispatch(hideLoading())
            })
            .catch(error => console.log("ERROR ACTION SEARCH MOVIE " + error))
    }
};
export const actionResetSearchMovie = () => {
    return dispatch => {
        dispatch(resetSearchList());
    }
};

const showMovieList = (results) => {
    return {
        type: ACTION_GET_UPCOMING_MOVIE,
        payload: {
            results
        }
    }
};
const getMoreMovieList = (results) => {
    return {
        type: ACTION_GET_MORE_UPCOMING_MOVIE,
        payload: {
            results
        }
    }
};
const searchList = (results) => {
    return {
        type: ACTION_SEARCH_MOVIE,
        payload: {
            results
        }
    }
};
const resetSearchList = () => {
    return {
        type: ACTION_RESET_SEARCH_MOVIE,
    }
};
const showLoading = () => {
    return {
        type: ACTION_SHOW_LOADING,
    }
};
const hideLoading = () => {
    return {
        type: ACTION_HIDE_LOADING
    }
};
const refreshMovieList = () => {
    return {
        type: ACTION_REFRESH_UPCOMING_MOVIE
    }
};