 import {
    ACTION_GET_UPCOMING_MOVIE, ACTION_GET_MORE_UPCOMING_MOVIE, ACTION_SEARCH_MOVIE,
    ACTION_RESET_SEARCH_MOVIE, ACTION_SHOW_LOADING, ACTION_HIDE_LOADING, ACTION_REFRESH_UPCOMING_MOVIE
} from "../Action";

export const movieState = {
    upComingMovies: [],
    extraUpComingMovies: [],
    listSearch: [],
    showLoading: true
};
export const movieReducer = (state = movieState, action) => {
    switch (action.type) {
        case ACTION_GET_UPCOMING_MOVIE:
            return {
                ...state,
                upComingMovies: [...action.payload.results],
            };
        case ACTION_GET_MORE_UPCOMING_MOVIE:
            return {
                ...state,
                extraUpComingMovies: [...state.extraUpComingMovies,...action.payload.results]
            };
        case ACTION_REFRESH_UPCOMING_MOVIE:
            return {
                ...state,
                upComingMovies:[],
                extraUpComingMovies:[]
            };
        case ACTION_SEARCH_MOVIE:
            console.log('SEARCH REDUCER ' + action.payload.results);
            return {
                ...state,
                listSearch: [...state.listSearch,...action.payload.results]
            };
        case ACTION_RESET_SEARCH_MOVIE:
            console.log("RESET SEARCH LIST");
            return {
                ...state,
                listSearch: []
            };
        case ACTION_SHOW_LOADING:
            console.log("SHOW LOADING");
            return {
                ...state,
                showLoading: true
            };
        case ACTION_HIDE_LOADING:
            console.log("HIDE LOADING");
            return {
                ...state,
                showLoading: false
            };
        default:
            return state;
    }
};
