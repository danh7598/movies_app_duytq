import React from 'react';
import {Platform, StatusBar, StatusBarIOS, Dimensions, Text} from "react-native";
import {sizeFont} from "./utils/Size";
export const POPULAR_MOVIE = "popular";
export const TOP_RATE_MOVIE = "top_rated";
export const UP_COMING_MOVIE = "upcoming";
export const NOW_PLAYING_MOVIE = "now_playing";

const X_WIDTH = 375;
const X_HEIGHT = 812;
const {height, width} = Dimensions.get('window');
const STATUS_BAR_IOS_HEIGHT = height === X_HEIGHT && width === X_WIDTH ? 40 : 20;
export const BOTTOM_BAR_IOS_HEIGHT = height === X_HEIGHT && width === X_WIDTH ? 30 : 0;

export const BASE_PATH_POSTER = "https://image.tmdb.org/t/p/w185";
export const BASE_PATH_BACK_DROP = "https://image.tmdb.org/t/p/w780";

export const PATH_CAST_PROFILE = "https://image.tmdb.org/t/p/w185";
export const PATH_PERSON = "https://image.tmdb.org/t/p/h632";

export const getYoutubeThumbnail = (videoKey) => {
    return 'http://img.youtube.com/vi/' + videoKey + '/0.jpg'
};
