import React, { PureComponent } from 'react';
import {
    StyleSheet,
    Text,
    View,
    BackHandler,
    Platform
} from 'react-native';
import AppMain from "./components/common/AppMain";
import {connect} from "react-redux";
import {goBack} from "./redux/router/NavigationAction";

class App extends PureComponent {

    constructor(props) {
        super(props);
    }

    componentWillMount() {
        BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
    }

    componentWillUnmount(){
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }

    shouldCloseApp = () => {
        return this.props.nav.index === 0;
    };

    handleBackPress = () => {
        if (this.shouldCloseApp()) {
            return false;
        }
        const {loading} = this.props;
        if (!loading) {
            this.props.goBack();
        }
        return true;
    };

    render() {
        return (
            <View style={styles.Container}>
                <AppMain/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    Container: {
        flex: 1
    }
});

export default connect(
    state => ({
        nav: state.nav
    }),
    {goBack}
)(App);
