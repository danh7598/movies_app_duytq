import React, {Component} from 'react';
import {View,Text,TextInput,StyleSheet,TouchableOpacity,ImageBackground,Image,
    ToastAndroid,ActivityIndicator,
    FlatList} from 'react-native';
import {connect} from 'react-redux';
import {sizeWidth,sizeHeight,sizeFont} from "../../../utils/Size";
import ToolBar from '../../common/ToolBar';
import {actionSearchMovie,actionResetSearchMovie} from "../../../redux/movie/MovieAction";
import {actionResetSearchTV,actionSearchTV} from "../../../redux/tv/TvAction";
import {navigateToPage} from "../../../redux/router/NavigationAction";
import PopularItemMovie from '../main/movie/PopularItemMovie';
import PopularItemTV from '../main/tv/PopularItemTV';
class Search extends Component{
    constructor(props){
        super(props);
        this.state = {
            searchData: '',
            index:2,
        }
    }
    componentDidMount(){
        this.props.actionResetSearchTV();
        this.props.actionResetSearchMovie();
    }
    renderNavigationBar = (type) => {
        return(
            <View style={styles.viewTitle}>
                <TouchableOpacity
                    onPress={() => {this.props.navigation.goBack()}}
                    style={styles.viewBackButton}>
                    <Image
                        style={styles.backButton}
                        source={require('../../../../resources/icon/left-arrow.png')}/>
                </TouchableOpacity>
                <View style={styles.viewTextInput}>
                    <Image
                    style={styles.searchImage}
                    source={require('../../../../resources/icon/search.png')}/>
                    <TextInput
                        style={styles.textInput}
                        onChangeText={(searchData) => this.setState({searchData})}
                        value={this.state.searchData}
                        returnKeyType={'search'}
                        placeholder={'Find your film'}
                        placeholderTextColor={'grey'}
                        underlineColorAndroid='rgba(0,0,0,0)'
                        onSubmitEditing={()=>{this.search(type)}}/>
                </View>
            </View>
        )
    };
    search = (type) => {
        if (type == 'Movie') {
            this.searchMovie();
        }
        else this.searchTV();
    };
    renderLoadingMovie = () => {
        const loading = this.props.showLoadingMovie;
        if (loading) {
            return (
                <ActivityIndicator
                    size={'large'}
                    color={'black'}
                />
            )
        }
    };
    renderLoadingTV = () => {
        const loading = this.props.showLoadingTV;
        if (loading) {
            return (
                <ActivityIndicator
                    size={'large'}
                    color={'black'}
                />
            )
        }
    };
    searchMovie = () => {
        this.props.actionResetSearchMovie();
        let uri = this.state.searchData;
        let res = encodeURI(uri);
        this.props.actionSearchMovie(res,1);
    };
    searchTV = () => {
        this.props.actionResetSearchTV();
        let uri = this.state.searchData;
        let res = encodeURI(uri);
        this.props.actionSearchTV(res,1);
    };
    _keyExtractor = (item) => item.id.toString();
    renderList = (type) => {
        if (type == 'Movie'){
            return(
                <View style={styles.ListPopular}>
                    {this.renderLoadingMovie()}
                    <FlatList
                        onEndReached={() => this.onEndReached()}
                        keyExtractor={this._keyExtractor}
                        horizontal={false}
                        numColumns={2}
                        showsVerticalScrollIndicator={false}
                        data={this.props.listSearch}
                        renderItem={({item,index}) => {
                            return(
                                <PopularItemMovie item={item} index={index}>
                                </PopularItemMovie>
                            )
                        }}>
                    </FlatList>
                </View>
            )
        }
        else {
            return(
                <View style={styles.ListPopular}>
                    {this.renderLoadingTV()}
                    <FlatList
                        onEndReached={() => this.onEndReached()}
                        keyExtractor={this._keyExtractor}
                        horizontal={false}
                        numColumns={2}
                        showsVerticalScrollIndicator={false}
                        data={this.props.listSearchTV}
                        renderItem={({item,index}) => {
                            return(
                                <PopularItemTV item={item} index={index}>
                                </PopularItemTV>
                            )
                        }}>
                    </FlatList>
                </View>
            )
        }
    };
    onEndReached = () => {
        this.setState({
            index:this.state.index + 1,
        });
        this.props.actionSearchMovie(this.state.index);
    };
    render(){
        const { params } = this.props.navigation.state;
        const type = params ? params.type : null;
        return(
            <View>
                <ToolBar/>
                {this.renderNavigationBar(type)}
                {this.renderList(type)}
            </View>
        )
    }
}
export default connect(state => ({
    nav: state.nav,
    listSearchTV: state.tvState.listSearchTV,
    listSearch: state.movieState.listSearch,
    showLoadingMovie: state.movieState.showLoading,
    showLoadingTV: state.tvState.showLoading
}),{navigateToPage,actionSearchMovie,
    actionResetSearchMovie,actionResetSearchTV,actionSearchTV})(Search);
const styles = StyleSheet.create({
    viewTitle:{
        flexDirection:'row',
        justifyContent:'flex-start',
        alignItems:'center',
        borderBottomWidth:1,
        height:sizeHeight(7),
        backgroundColor:'white'
    },
    viewBackButton:{
        alignItems:'center',
        justifyContent:'center',
        marginLeft:sizeWidth(6),
        width:sizeWidth(8),
        height:sizeWidth(6)
    },
    backButton:{
        width:sizeWidth(6),
        height:sizeWidth(5),
    },
    searchImage:{
        marginLeft:sizeWidth(1),
        width:sizeWidth(5),
        height:sizeWidth(5),
        tintColor:'grey'
    },
    viewTextInput:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'flex-start',
        borderRadius:10,
        borderWidth:1,
        width:sizeWidth(80),
        height:sizeHeight(5),
    },
    textInput:{
        width:sizeWidth(70),
        height:sizeHeight(10),
        marginLeft:sizeWidth(2),
        color:'black',
        fontSize:sizeFont(4)
    },
    ListPopular:{
        marginTop:sizeHeight(0.5),
        marginLeft:sizeWidth(4),
        marginRight:sizeWidth(4),
        marginBottom:sizeHeight(20),
    },
});