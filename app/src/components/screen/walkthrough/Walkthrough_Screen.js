import React,{Component} from 'react';
import {
    View, Text, TouchableOpacity, Image, ImageBackground, StyleSheet, AsyncStorage
} from 'react-native';
import Swiper from 'react-native-swiper';
import {sizeWidth,sizeHeight,sizeFont} from "../../../utils/Size";
import {connect} from 'react-redux';
import {navigateToPage, goBack} from "../../../redux/router/NavigationAction";
class WalkThrough extends Component {
    save = async() => {
        try{
            await AsyncStorage.setItem("@PAGEMAIN:key",'Did WalkThrough');
            console.log('SET OK')
        }catch(e){
            console.log(e)
        }
    };
    renderSlide1 = () => {
        return(
            <ImageBackground
                style={styles.slide1}
                source={require('../../../../resources/walkthrough/walkthrough1.png')}>
                <Text style={styles.firstTextSlide}>
                    Get the first
                </Text>
                <Text style={styles.nextTextSlide}>
                    Movie & TV
                </Text>
                <Text style={styles.nextTextSlide}>
                    information
                </Text>
            </ImageBackground>
        )
    };
    renderSlide2 = () => {
        return(
            <ImageBackground
                style={styles.slide2}
                source={require('../../../../resources/walkthrough/walkthrough2.png')}>
                <Text style={styles.firstTextSlide}>
                    Know the movie
                </Text>
                <Text style={styles.nextTextSlide}>
                    is not worth
                </Text>
                <Text style={styles.nextTextSlide}>
                    Watching
                </Text>
            </ImageBackground>
        )
    };
    renderSlide3 = () => {
        return(
            <ImageBackground
                style={styles.slide3}
                source={require('../../../../resources/walkthrough/walkthrough3.png')}>
                <Text style={styles.firstTextSlide}>
                    Real-time
                </Text>
                <Text style={styles.nextTextSlide}>
                    updates movie
                </Text>
                <Text style={styles.nextTextSlide}>
                    Trailer
                </Text>
                <TouchableOpacity
                    onPress={()=> this.goToHome()}>
                    <ImageBackground
                        imageStyle={{borderRadius:10}}
                        source={require('../../../../resources/walkthrough/next.png')}
                        style={styles.buttonStart}
                        resizeMode={'stretch'}>
                        <Text style={styles.textButton}>
                            Get Started
                        </Text>
                    </ImageBackground>
                </TouchableOpacity>
            </ImageBackground>

        )
    };
    goToHome = () => {
        this.save();
        this.props.navigateToPage('Login');
    };
    render(){
        return(
            <Swiper dot={this.renderDot()}
                    activeDot={this.renderActiveDot()}
                    loop={false}>
                {this.renderSlide1()}
                {this.renderSlide2()}
                {this.renderSlide3()}
            </Swiper>
        )
    }
    renderDot = () => {
        return(
            <View style={{backgroundColor:'#ffffff',
                    width: 13, height: 13,
                    borderRadius: 10, marginLeft: 4,
                    marginRight: 4, marginTop: 3,
                    marginBottom: 3,}} />
        )
    };
    renderActiveDot = () => {
        return(
            <View style={{backgroundColor: '#fffb10',
                width: 13, height: 13,
                borderRadius: 10, marginLeft: 4,
                marginRight: 4, marginTop: 3,
                marginBottom: 3,}} />
        )
    };
}
export default connect(null, {navigateToPage})(WalkThrough)
const styles = StyleSheet.create({
    slide1:{
        width:sizeWidth(100),
        height:sizeHeight(100),
        alignItems:'center'
    },
    slide2:{
        width:sizeWidth(100),
        height:sizeHeight(100),
        alignItems:'center'
    },
    slide3:{
        width:sizeWidth(100),
        height:sizeHeight(100),
        alignItems:'center'
    },
    firstTextSlide:{
        marginTop:sizeHeight(50),
        fontSize:sizeFont(10),
        color:'white'
    },
    nextTextSlide:{
        fontSize:sizeFont(10),
        color:'white'
    },
    buttonStart:{
        width:sizeWidth(40),
        height:sizeHeight(7),
        marginTop:sizeHeight(7),
        borderRadius:20,
        justifyContent:'center',
        alignItems:'center'
    },
    textButton:{
        fontSize:sizeFont(6),
        color:'white'
    },
    backgroundButton:{
        flex:1
    },
    paginationView:{
        backgroundColor:'white'
    },
});