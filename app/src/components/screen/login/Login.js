import React, {Component} from 'react';
import {
    View, Text, TextInput, StyleSheet, TouchableOpacity, ImageBackground, Image, ToastAndroid,
    AsyncStorage
} from 'react-native';
import {connect} from 'react-redux';
import {sizeWidth,sizeHeight,sizeFont} from "../../../utils/Size";
import {navigateToPage} from "../../../redux/router/NavigationAction";
class Login extends Component{
    constructor(props){
        super(props);
        this.state = {
            loginName:'',
            loginPass:''
        }
    }
    renderToolBar = () => {
        return(
            <View style={styles.viewToolBar}/>
        )
    };
    renderBackground = () => {
        return(
            <View>
                <ImageBackground
                    style={styles.background}
                    source={require('../../../../resources/login/login.png')}>
                    {this.renderToolBar()}
                    {this.renderLoginName()}
                    {this.renderLoginPass()}
                    {this.renderButtonLogin()}
                </ImageBackground>

            </View>
        )
    };
    renderLoginName = () => {
        return(
           <View style={styles.viewLoginName}>
                <TextInput
                    style={styles.loginName}
                    autoCapitalize={'none'}
                    placeholder={'Input Email'}
                    keyboardType={'email-address'}
                    placeholderTextColor={'white'}
                    onChangeText={(loginName) => this.setState({loginName})}
                    value={this.state.loginName}
                    underlineColorAndroid='rgba(0,0,0,0)'/>
           </View>
        )
    };
    renderLoginPass = () => {
        return(
            <View style={styles.viewLoginPass}>
                <TextInput
                    autoCapitalize={'none'}
                    style={styles.loginPass}
                    placeholder={'Input password'}
                    placeholderTextColor={'white'}
                    onChangeText={(loginPass) => this.setState({loginPass})}
                    value={this.state.loginPass}
                    secureTextEntry={true}
                    underlineColorAndroid='rgba(0,0,0,0)'/>

            </View>
        )
    };
    set = async() => {
        const TotalLike = [];
        const numberCount = {
            like:0,
            watch:0,
            comment:0
        };
        try{
            AsyncStorage.getItem("@TOTALLIKE:key", (err, result) => {
                if (result == null) {
                    AsyncStorage.setItem("@TOTALLIKE:key",JSON.stringify(TotalLike));
                    console.log("CREATE TOTAL LIKE OK")
                }
            });
            AsyncStorage.getItem("@NUMBERCOUNT:key", (err,result) => {
                if (result == null) {
                    AsyncStorage.setItem("@NUMBERCOUNT:key",JSON.stringify(numberCount));
                    console.log("CREATE NUMBER COUNT OK")
                }
            })
        }catch(e){
            console.log(e)
        }
    };
    goToMain = () => {
        const name = this.state.loginName;
        const pass = this.state.loginPass
        if((name  === 'admin') && (pass === 'admin')){
            this.set();
            this.props.navigateToPage('Main')
        }
        else {
            ToastAndroid.show('Wrong name or password !', ToastAndroid.SHORT);
        }
    };

    renderButtonLogin = () => {
        return(
            <TouchableOpacity
                onPress={()=> this.goToMain()}
                style={styles.buttonLogin}>
                <Text style={styles.textLoginButton}>
                    Login
                </Text>
            </TouchableOpacity>
        )
    };
    render(){
        return(
            <View>
                {this.renderBackground()}
            </View>
        )
    }
}
export default connect(null,{navigateToPage})(Login);
const styles = StyleSheet.create({
    viewToolBar:{
        height:sizeHeight(3.3),
        backgroundColor:'black'
    },
    viewLoginName:{
        marginLeft:sizeWidth(10),
        marginRight:sizeWidth(10),
        borderBottomWidth:1,
        borderColor:'#2f3d08'
    },
    loginName:{
        height:sizeHeight(10),
        marginTop:sizeHeight(25),
        padding:sizeWidth(2),
        fontSize:sizeFont(5),
        color:'white'
    },
    loginPass:{
        height:sizeHeight(10),
        marginLeft:sizeWidth(10),
        marginRight:sizeWidth(10),
        padding:sizeWidth(2),
        fontSize:sizeFont(5),
        color:'white'
    },
    viewLoginPass:{
    },
    buttonLogin:{
        height:sizeHeight(7),
        marginTop:sizeHeight(5),
        marginLeft:sizeWidth(10),
        marginRight:sizeWidth(10),
        padding:sizeWidth(2),
        borderRadius:20,
        borderWidth:1,
        justifyContent:'center',
        alignItems:'center',
        borderColor:'white'
    },
    textLoginButton:{
        fontSize:sizeFont(5),
        fontWeight:'bold',
        color:'white'
    },
    background:{
        width:sizeWidth(100),
        height:sizeHeight(100)
    }

});