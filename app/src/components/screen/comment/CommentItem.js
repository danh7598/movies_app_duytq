import React,{Component} from 'react';
import {
    View,Text,TouchableOpacity,Image,ImageBackground,StyleSheet,FlatList,ScrollView,AsyncStorage
} from 'react-native';
import {sizeWidth,sizeHeight,sizeFont} from "../../../utils/Size";
import ViewMoreText from 'react-native-view-more-text';
import {connect} from 'react-redux';
import {navigateToPage, goBack} from "../../../redux/router/NavigationAction";
class CommentItem extends Component {
    render(){
        return(
            <View style={styles.viewCommentItem}>
                <Text style={styles.textComment}>
                    {this.props.item}
                </Text>
            </View>
        )
    }
}
export default connect(null,{navigateToPage})(CommentItem);
const styles = StyleSheet.create({
    viewCommentItem:{
        height:sizeHeight(7),
        justifyContent:'center',
        alignItems:'flex-start',
        paddingLeft:sizeWidth(2),
        backgroundColor:'white',
        borderBottomWidth:0.5,
        borderTopWidth:0.5
    },
    textComment:{
        fontSize:sizeFont(5),
        color:'black'
    }
});