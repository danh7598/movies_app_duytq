import React,{Component} from 'react';
import {
    View,Text,TouchableOpacity,Image,ImageBackground,StyleSheet,FlatList,ScrollView,AsyncStorage,TextInput,Keyboard
} from 'react-native';
import {sizeWidth,sizeHeight,sizeFont} from "../../../utils/Size";
import Toolbar from '../../common/ToolBar';
import ViewMoreText from 'react-native-view-more-text';
import {connect} from 'react-redux';
import {navigateToPage, goBack} from "../../../redux/router/NavigationAction";
import CommentItem from './CommentItem';
let dataComment = [];
class CommentScreen extends Component {
    constructor(props){
        super(props);
        this.state = {
            commentText:'',
            keyboardComment:false,
            comment:false,
            dataComment:[],
        }
    }
    componentDidMount () {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
    }
    _keyboardDidShow = () => {
        this.setState({
            keyboardComment:true
        })
    };
    _keyboardDidHide = () => {
        this.setState({
            keyboardComment:false
        })
    };
    renderNavigationBar = () => {
        return(
            <View style={styles.viewTitle}>
                <TouchableOpacity
                    onPress={() => {this.props.navigation.goBack()}}
                    style={styles.viewBackButton}>
                    <Image
                        style={styles.backButton}
                        source={require('../../../../resources/icon/left-arrow.png')}/>
                </TouchableOpacity>
                <Text style={styles.textTitle}>
                    Comment
                </Text>
            </View>
        )
    };
    renderComment = (item) => {
        if (this.state.keyboardComment == false) {
            return this.renderCommentWithoutKeyboard(item)
        }
        else return this.renderCommentWithKeyboard(item)
    };
    getResult = async(item) => {
        try{
            const data = await AsyncStorage.getItem("@ID" + item.id +"COMMENT" + ":key");
            dataComment = JSON.parse(data);
            console.log('PROF' + dataComment);
            this.setState({
                dataComment:dataComment
            })
        }catch(e){
            console.log(e)
        }
    };
    renderCommentWithKeyboard = (item) => {
        if(this.state.comment == false) {
            return (
                <View style={{justifyContent: 'flex-end'}}>
                    <View style={styles.viewListCommentWithKeyboard}>
                        {this.renderEmptyComment()}
                    </View>
                    <View style={styles.viewComment}>
                        <TextInput
                            style={styles.textCommentInput}
                            onChangeText={(commentText) => this.setState({commentText})}
                            value={this.state.commentText}
                            placeholder={'Comment'}
                            placeholderTextColor={'grey'}
                            underlineColorAndroid='rgba(0,0,0,0)'/>
                        <TouchableOpacity
                            onPress={() => {
                                this.setComment(item)}}>
                            <Image
                                style={styles.sendButton}
                                source={require('../../../../resources/icon/send-button.png')}/>
                        </TouchableOpacity>
                    </View>
                </View>
            )
        }
        else {
            this.getResult(item);
            return(
                <View style={{justifyContent:'flex-end'}}>
                    <View  style={styles.viewCommentWithKeyboard}>
                        {this.renderListComment()}
                    </View>
                    <View style={styles.viewComment}>
                        <TextInput
                            style={styles.textCommentInput}
                            onChangeText={(commentText) => this.setState({commentText})}
                            value={this.state.commentText}
                            placeholder={'Comment'}
                            placeholderTextColor={'grey'}
                            underlineColorAndroid='rgba(0,0,0,0)'/>
                        <TouchableOpacity
                            onPress={() => {this.setComment(item)}}>
                            <Image
                                style={styles.sendButton}
                                source={require('../../../../resources/icon/send-button.png')}/>
                        </TouchableOpacity>
                    </View>
                </View>
            )
        }
    };
    renderCommentWithoutKeyboard = (item) => {
        if(this.state.comment == false){
            return(
                <View style={{justifyContent:'flex-end'}}>
                    <View  style={styles.viewListCommentWithoutKeyboard}>
                        {this.renderEmptyComment()}
                    </View>
                    <View style={styles.viewComment}>
                        <TextInput
                            style={styles.textCommentInput}
                            onChangeText={(commentText) => this.setState({commentText})}
                            value={this.state.commentText}
                            placeholder={'Comment'}
                            placeholderTextColor={'grey'}
                            underlineColorAndroid='rgba(0,0,0,0)'/>
                        <TouchableOpacity
                            onPress={() => {this.setComment(item)}}>
                            <Image
                                style={styles.sendButton}
                                source={require('../../../../resources/icon/send-button.png')}/>
                        </TouchableOpacity>
                    </View>
                </View>
            )
        }
        else {
            this.getResult(item);
            return(
                <View style={{justifyContent:'flex-end'}}>
                    <View  style={styles.viewCommentWithoutKeyboard}>
                        {this.renderListComment()}
                    </View>
                    <View style={styles.viewComment}>
                        <TextInput
                            style={styles.textCommentInput}
                            onChangeText={(commentText) => this.setState({commentText})}
                            value={this.state.commentText}
                            placeholder={'Comment'}
                            placeholderTextColor={'grey'}
                            underlineColorAndroid='rgba(0,0,0,0)'/>
                        <TouchableOpacity
                            onPress={() => {this.setComment(item)}}>
                            <Image
                                style={styles.sendButton}
                                source={require('../../../../resources/icon/send-button.png')}/>
                        </TouchableOpacity>
                    </View>
                </View>
            )
        }

    };
    countComment = async() => {
        try {
                AsyncStorage.getItem("@NUMBERCOUNT:key",(err,result) => {
                const count = JSON.parse(result);
                const Comment = {
                    comment: count.comment + 1
                };
                AsyncStorage.mergeItem("@NUMBERCOUNT:key",JSON.stringify(Comment));
                console.log("NUMBER COUNT" + JSON.stringify(AsyncStorage.getItem("@NUMBERCOUNT:key")))
            });
        }
        catch(e){
            console.log(e)
        }
    };
    renderEmptyComment = () => {
        console.log('EMPTY COMMENT');
        return(
            <View style={styles.viewEmptyComment}>
                <Text style={styles.textEmptyComment}>
                    No comment yet
                </Text>
            </View>
        )
    };
    setComment = async(item) => {
        try{
            const comment = [
            ];
            if (this.state.commentText.length > 0) {
                AsyncStorage.getItem("@ID" + item.id +"COMMENT" + ":key",(err, result) => {
                    if(result == null){
                        AsyncStorage.setItem("@ID" + item.id +"COMMENT" + ":key",JSON.stringify(comment));
                        console.log('CREATE COMMENT OK');
                        AsyncStorage.getItem("@ID" + item.id +"COMMENT" + ":key",(err,results) => {
                            const TotalComment = JSON.parse(results);
                            TotalComment.push(this.state.commentText);
                            console.log('CREATE COMMENT OK 2 ' + TotalComment);
                            AsyncStorage.setItem("@ID" + item.id +"COMMENT" + ":key",JSON.stringify(TotalComment));
                            this.setState({
                                commentText:''
                            });
                            this.countComment();

                        })
                    }
                    else {
                        const TotalComment = JSON.parse(result);
                        console.log('CREATE COMMENT OK 3 ' + result);
                        TotalComment.push(this.state.commentText);
                        AsyncStorage.setItem("@ID" + item.id +"COMMENT" + ":key",JSON.stringify(TotalComment));
                        this.setState({
                            commentText:''
                        });
                        this.countComment()
                    }
                })
            }
        }catch(e){
            console.log(e)
        }
    };
    getComment = async(item) => {
        try{
            AsyncStorage.getItem("@ID" + item.id +"COMMENT" + ":key",(err, result) => {
                console.log('GET COMMENT ' + result);
                if(result != null){
                    console.log('RENDER NULL');
                    if(this.state.comment == false){
                        this.setState({
                            comment:true
                        });
                    }
                }
            })
        }catch(e){
            console.log(e)
        }
    };
    renderListComment = () => {
        return(
            <View>
                {console.log('OKASD', this.state.dataComment)}
                <FlatList
                    ref={'ListComment'}
                    showsVerticalScrollIndicator={false}
                    data={this.state.dataComment}
                    renderItem={({item,index}) => {
                        console.log('OKASDABC', JSON.stringify(item));
                        return(
                            <CommentItem item={item} index={index}>
                            </CommentItem>
                        )}}/>
            </View>
        )
    };
    render(){
        const { params } = this.props.navigation.state;
        const item = params ? params.information : null;
        console.log('ITEM COMMENT GET ' + JSON.stringify(item));
        this.getComment(item);
        return(
            <View>
                <Toolbar/>
                <View>
                    {this.renderNavigationBar()}
                    {this.renderComment(item)}
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    viewTitle:{
        flexDirection:'row',
        justifyContent:'flex-start',
        alignItems:'center',
        borderBottomWidth:1,
        height:sizeHeight(7),
        backgroundColor:'white'
    },
    viewBackButton:{
        alignItems:'center',
        justifyContent:'center',
        marginLeft:sizeWidth(6),
        width:sizeWidth(8),
        height:sizeWidth(6)
    },
    backButton:{
        width:sizeWidth(6),
        height:sizeWidth(5),
    },
    textTitle:{
        marginLeft:sizeWidth(6),
        fontSize:sizeFont(6),
        color:'black'
    },
    textCommentInput:{
        width:sizeWidth(74),
        height:sizeHeight(11),
        marginLeft:sizeWidth(2),
        color:'black',
        fontSize:sizeFont(5),
    },
    viewComment:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        borderBottomWidth:1,
        borderTopWidth:1,
        height:sizeHeight(8),
        backgroundColor:'white',
    },
    viewListCommentWithoutKeyboard:{
        height:sizeHeight(81.6),
        borderWidth:1,
        borderRightWidth:0,
        borderLeftWidth:0,
        backgroundColor:'white'
    },
    viewListCommentWithKeyboard:{
        height:sizeHeight(45.4),
        borderWidth:1,
        borderRightWidth:0,
        borderLeftWidth:0,
        backgroundColor:'white'
    },
    sendButton:{
        height:sizeWidth(6),
        width:sizeWidth(6),
        marginRight:sizeWidth(6)
    },
    viewEmptyComment:{
        marginTop:sizeHeight(10),
        alignItems:'center'
    },
    textEmptyComment:{
        color:'black',
        fontSize:sizeFont(6),
        fontWeight:'bold'
    },
    viewCommentWithKeyboard:{
        margin:sizeWidth(2),
        borderWidth:1,
        borderTopWidth:0,
        backgroundColor:'white',
        height:sizeHeight(43),
    },
    viewCommentWithoutKeyboard:{
        height:sizeHeight(79),
        margin:sizeWidth(2),
        borderWidth:1,
        borderTopWidth:0,
        backgroundColor:'white'
    }
});
export default connect(null,{navigateToPage,goBack})(CommentScreen);
