import React,{Component} from 'react';
import {
    View,Text,TouchableOpacity,Image,ImageBackground,StyleSheet,FlatList,ScrollView,Dimensions
} from 'react-native';
import {sizeWidth,sizeHeight,sizeFont} from "../../../../utils/Size";
import {connect} from 'react-redux';
import {navigateToPage, goBack} from "../../../../redux/router/NavigationAction";
import {BASE_PATH_POSTER} from "../../../../Constant";
class PopularItemTV extends Component{
    render(){
        const tv = this.props.item;
        const name = tv && tv.name;
        const poster_path = tv && tv.poster_path;
        const imageUri = BASE_PATH_POSTER + poster_path;
        return(
            <TouchableOpacity
                onPress={()=>{this.goToTvItem()}}
                style={styles.TvItem}>
                <Image
                    style={styles.imagePopularItem}
                    source={{uri:imageUri}}
                    resizeMode={'cover'}/>
                <View style={styles.viewTextTvItem}>
                    <Text
                        numberOfLines={1}
                        style={styles.textTvItem}>{name}</Text>
                </View>
            </TouchableOpacity>
        )
    }
    goToTvItem = () => {
        const information = this.props.item;
        this.props.navigateToPage('TVDetail',{information});
    }
}

export default connect(null,{navigateToPage})(PopularItemTV);
const styles = StyleSheet.create({
    TvItem:{
        justifyContent:'center',
        alignItems:'center',
        borderRadius:5,
        marginLeft:sizeWidth(2),
        marginBottom:sizeHeight(1),
        backgroundColor:'white'
    },
    imagePopularItem:{
        marginLeft:sizeWidth(2),
        marginRight:sizeWidth(2),
        marginTop:sizeWidth(2),
        width:sizeWidth(43),
        height:sizeHeight(35),
        borderRadius:5,
    },
    viewTextTvItem:{
        justifyContent:'center',
        alignItems:'center',
        width:sizeWidth(35)
    },
    textTvItem:{
        fontSize:sizeWidth(4.5),
        color:'black',
        marginTop:sizeHeight(0.3),
        textAlign:'center'
    },
});