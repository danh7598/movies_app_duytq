import React,{Component} from 'react';
import {
    View,Text,TouchableOpacity,Image,ImageBackground,StyleSheet,FlatList,ScrollView
} from 'react-native';
import {sizeWidth,sizeHeight,sizeFont} from "../../../../utils/Size";
import {connect} from 'react-redux';
import {navigateToPage, goBack} from "../../../../redux/router/NavigationAction";
import Toolbar from '../../../common/ToolBar';
import {actionGetTVList,actionGetMoreTVList,actionRefreshTVList} from "../../../../redux/tv/TvAction";
import PopularItemTV from './PopularItemTV';
class PopularTVList extends Component{
    constructor(props){
        super(props);
        this.state = {
            isRefreshing: false,
            index:2,
        }
    }
    componentDidMount() {
        this.props.actionGetMoreTVList(1);
    }
    _keyExtractor = (item, index) => item.id.toString();
    onRefresh() {
        this.setState({
            isRefreshing: true,
        }, function() {
                this.afterPull();
            });
    }
    afterPull = () => {
        this.props.actionRefreshTVList();
        this.props.actionGetMoreTVList(1);
        this.setState({
            index:2,
            isRefreshing: false,
        });
    };
    onEndReached = () => {
        this.setState({
            index:this.state.index + 1,
        });
        this.props.actionGetMoreTVList(this.state.index);
    };
    renderList = () => {
        return(
            <View>
                <View style={styles.viewTitle}>
                    <TouchableOpacity
                        onPress={() => {this.props.navigation.goBack()}}
                        style={styles.viewBackButton}>
                        <Image
                            style={styles.backButton}
                            source={require('../../../../../resources/icon/left-arrow.png')}/>
                    </TouchableOpacity>
                    <Text style={styles.textTitle}>
                        Top Rate TV
                    </Text>
                </View>
                <View style={styles.viewFlatListTV}>
                    <FlatList
                        showsVerticalScrollIndicator={false}
                        onEndReached={() => this.onEndReached()}
                        onEndReachedThreshold={5}
                        keyExtractor={this._keyExtractor}
                        onRefresh={() => this.onRefresh()}
                        refreshing={this.state.isRefreshing}
                        numColumns={2}
                        data={this.props.extraTopRateTv}
                        renderItem={({item,index}) => {
                            return(
                                <PopularItemTV item={item} index={index}>
                                </PopularItemTV>
                            )
                        }}>
                    </FlatList>
                </View>
            </View>
        )
    };
    render(){
        return(
            <View style={{flex:1}}>
                <Toolbar/>
                <View style={{flex:1}}>
                    {this.renderList()}
                </View>
            </View>
        )
    }
}
export default connect(state => ({
    nav: state.nav,
    extraTopRateTv: state.tvState.extraTopRateTv,
    showLoading: state.tvState.showLoading
}), {navigateToPage, goBack,actionGetMoreTVList,actionRefreshTVList})(PopularTVList);
const styles = StyleSheet.create({
    textTitle:{
        marginLeft:sizeWidth(6),
        fontSize:sizeFont(6),
        color:'black'
    },
    viewBackButton:{
        alignItems:'center',
        justifyContent:'center',
        marginLeft:sizeWidth(6),
        width:sizeWidth(8),
        height:sizeWidth(6)
    },
    backButton:{
        width:sizeWidth(6),
        height:sizeWidth(5),
    },
    viewItem:{
        justifyContent:'center',
        alignItems:'center',
        height:sizeHeight(45),
    },
    viewTitle:{
        flexDirection:'row',
        justifyContent:'flex-start',
        alignItems:'center',
        borderBottomWidth:0.3,
        height:sizeHeight(7),
        backgroundColor:'white'
    },
    textItem:{
        textAlign:'center',
        fontSize:sizeFont(4),
        color:'black',
        marginTop:sizeHeight(0.3)
    },
    imageItem: {
        width:sizeWidth(38),
        height:sizeHeight(39),
        borderRadius:5
    },
    viewFlatListTV:{
        marginTop:sizeHeight(2),
        marginLeft:sizeWidth(4),
        marginRight:sizeWidth(4)
    },

});