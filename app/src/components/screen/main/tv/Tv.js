import React,{Component} from 'react';
import {
    View, Text, TouchableOpacity, Image, ImageBackground, StyleSheet, FlatList, ScrollView, ActivityIndicator,
    BackHandler
} from 'react-native';
import {sizeWidth,sizeHeight,sizeFont} from "../../../../utils/Size";
import {connect} from 'react-redux';
import {navigateToPage, goBack} from "../../../../redux/router/NavigationAction";
import Toolbar from '../../../common/ToolBar';
import {actionGetTVList,actionRefreshTVList} from "../../../../redux/tv/TvAction";
import PopularItemTV from './PopularItemTV';
class Television extends Component {
    constructor(props){
        super(props);
        this.state = {
            isRefreshing:false
        }
    }
    componentWillMount() {
        this.props.actionGetTVList(1);
    }
    componentDidMount() {

    }
    componentWillUnmount(){
        BackHandler.exitApp()
    }
    _keyExtractor = (item) => item.id.toString();
    renderTVTitle = () => {
        return(
            <View>
                <View style={styles.TVTitle}>
                    <Text style={styles.textTVTitle}>
                        TV
                    </Text>
                    <TouchableOpacity
                        onPress={() => {this.goToSearch()}}
                        style={styles.searchView}>
                        <Image
                            style={styles.searchButton}
                            source={require('../../../../../resources/icon/search.png')}/>
                    </TouchableOpacity>
                </View>
            </View>
        )
    };
    goToPopularList = () => {
        this.props.navigateToPage('PopularTVList')
    };
    goToSearch = () => {
        const type = 'TV';
        this.props.navigateToPage('Search',{type})
    };
    renderLoading = () => {
        const loading = this.props.showLoading;
        if (loading) {
            return (
                <ActivityIndicator
                    size={'large'}
                    color={'black'}
                />
            )
        }
    };
    onRefresh() {
        this.setState({
            isRefreshing: true,
        }, function() {
            this.afterPull();
        });
    }
    afterPull = () => {
        this.props.actionRefreshTVList();
        this.props.actionGetTVList(1);
        this.setState({
            isRefreshing: false,
        });
    };
    renderPopularList = () => {
        return(
            <View>
                <View style={styles.popularTitle}>
                    <Text style={styles.textTitle}>
                        Top Rate TV
                    </Text>
                    <TouchableOpacity
                        onPress={() => this.goToPopularList()}
                        style={styles.buttonMORE}>
                        <Text style={styles.textButtonMORE}>
                            MORE
                        </Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.listTV}>
                    {this.renderLoading()}
                    <FlatList
                        onRefresh={() => this.onRefresh()}
                        refreshing={this.state.isRefreshing}
                        keyExtractor={this._keyExtractor}
                        showsVerticalScrollIndicator={false}
                        horizontal={false}
                        numColumns={2}
                        data={this.props.topRateTv}
                        renderItem={({item,index}) => {
                            return(
                                <PopularItemTV item={item} index={index}>
                                </PopularItemTV>
                            )
                        }}>
                    </FlatList>
                </View>
            </View>
        )
    };
    render(){
        return(
            <View style={{flex:1}}>
                <Toolbar/>
                <View style={{flex:1}}>
                    {this.renderTVTitle()}
                    {this.renderPopularList()}
                </View>
            </View>
        )
    }
}
export default connect(state => ({
    nav: state.nav,
    topRateTv: state.tvState.topRateTv,
    extraTopRateTv: state.tvState.extraTopRateTv,
    showLoading: state.tvState.showLoading
}), {navigateToPage, goBack,actionGetTVList,actionRefreshTVList})(Television);
const styles = StyleSheet.create({
    TVTitle:{
        flexDirection:'row',
        borderBottomWidth:0.3,
        backgroundColor:'white'
    },
    popularTitle:{
        backgroundColor:'white',
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',
        marginTop:sizeHeight(2),
        borderRadius:5,
        margin:sizeWidth(2),
        height:sizeHeight(6)
    },
    buttonMORE:{
        marginRight:sizeWidth(4),
        borderRadius:5,
    },
    textButtonMORE:{
        fontSize:sizeFont(4),
        fontWeight:'bold',
        color:'black',
        padding:3
    },
    searchView:{
        marginTop:sizeHeight(1),
        alignItems:'flex-end',
        justifyContent:'center',
        width:sizeWidth(75)
    },
    searchButton:{
        width:sizeWidth(7),
        height:sizeWidth(7)
    },
    textTVTitle:{
        fontSize:sizeFont(10),
        marginTop:sizeHeight(1),
        marginLeft:sizeWidth(6),
        color:'black'
    },
    nowList: {
        height: sizeHeight(40),
    },
    textTitle:{
        marginLeft:sizeWidth(4),
        fontSize:20,
        color:'black'
    },
    TVItem:{
        justifyContent:'center',
        alignItems:'center',
    },
    PopularItem:{
        justifyContent:'flex-start',
        alignItems:'flex-start'
    },
    imagePopularItem:{
        width:sizeWidth(88),
        height:sizeHeight(20),
        borderRadius:5
    },
    imageTVItem:{
        width:sizeWidth(36),
        height:sizeHeight(33),
    },
    textTVItem:{
        fontSize:sizeFont(4),
        color:'black',
        marginTop:sizeHeight(0.3),
        textAlign:'center'
    },
    FlatListTV:{
        flexDirection:'row',
        marginTop:sizeHeight(1),
        marginLeft:sizeWidth(6),
        marginEnd:sizeWidth(4)
    },
    FlatListPopular:{
        marginTop:sizeHeight(1),
        marginLeft:sizeWidth(4),
    },
    listTV:{
        backgroundColor:'transparent',
        marginTop:sizeHeight(0.5),
        marginRight:sizeWidth(2),
        marginBottom:sizeHeight(0.5)
    },
}) ;