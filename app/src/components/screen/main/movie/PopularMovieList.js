import React,{Component} from 'react';
import {
    View,Text,TouchableOpacity,Image,StyleSheet,FlatList,ActivityIndicator
} from 'react-native';
import {sizeWidth,sizeHeight,sizeFont} from "../../../../utils/Size";
import {connect} from 'react-redux';
import {navigateToPage, goBack} from "../../../../redux/router/NavigationAction";
import Toolbar from '../../../common/ToolBar';
import {actionGetMoreMovieList,actionRefreshMovieList} from "../../../../redux/movie/MovieAction";
import PopularItemMovie from './PopularItemMovie';
class PopularMovieList extends Component{
    constructor(props){
        super(props);
        this.state = {
            isRefreshing: false,
            index:2,
        }
    }
    componentDidMount() {
        this.props.actionGetMoreMovieList(1);
    }
    renderLoading = () => {
        const loading = this.props.showLoading;
        if (loading) {
            return (
                <ActivityIndicator
                    size={'large'}
                    color={'black'}
                />
            )
        }
    };
    _keyExtractor = (item) => item.id.toString();
    onRefresh() {
        this.setState({
            isRefreshing: true,
        }, function() {
            this.afterPull();
        });
    }
    afterPull = () => {
        this.props.actionRefreshMovieList();
        this.props.actionGetMoreMovieList(1);
        this.setState({
            index:2,
            isRefreshing: false,
        });
    };
    onEndReached = () => {
        this.setState({
            index:this.state.index + 1,
        });
        this.props.actionGetMoreMovieList(this.state.index);
    };
    renderList = () => {
        return(
            <View>
                <View style={styles.viewTitle}>
                    <TouchableOpacity
                        onPress={() => {this.props.navigation.goBack()}}
                        style={styles.viewBackButton}>
                        <Image
                            style={styles.backButton}
                            source={require('../../../../../resources/icon/left-arrow.png')}/>
                    </TouchableOpacity>
                    <Text style={styles.title}>
                        Upcoming Movie
                    </Text>
                </View>
                <View style={styles.viewFlatList}>
                    {this.renderLoading()}
                    <FlatList
                        showsVerticalScrollIndicator={false}
                        onEndReached={() => this.onEndReached()}
                        onEndReachedThreshold={5}
                        keyExtractor={this._keyExtractor}
                        onRefresh={() => this.onRefresh()}
                        refreshing={this.state.isRefreshing}
                        numColumns={2}
                        data={this.props.extraUpComingMovies}
                        renderItem={({item,index}) => {
                            return(
                                <PopularItemMovie item={item} index={index}>
                                </PopularItemMovie>
                            )
                        }}>
                    </FlatList>
                </View>
            </View>
        )
    };
    render(){
        console.log('OASD' + this.props.extraUpComingMovies);
        return(
            <View style={{flex:1}}>
                <Toolbar/>
                <View style={{flex:1}}>
                    {this.renderList()}
                </View>
            </View>
        )
    }
}
export default connect(state => ({
    nav: state.nav,
    extraUpComingMovies: state.movieState.extraUpComingMovies,
    showLoading: state.movieState.showLoading
}), {navigateToPage, goBack, actionGetMoreMovieList,
    actionRefreshMovieList})(PopularMovieList);
const styles = StyleSheet.create({
    title:{
        marginLeft:sizeWidth(6),
        fontSize:sizeFont(6),
        color:'black'
    },
    viewBackButton:{
        alignItems:'center',
        justifyContent:'center',
        marginLeft:sizeWidth(6),
        width:sizeWidth(8),
        height:sizeWidth(6)
    },
    backButton:{
        width:sizeWidth(6),
        height:sizeWidth(5),
    },
    viewItem:{
        justifyContent:'center',
        alignItems:'center',
        height:sizeHeight(45),
    },
    viewTitle:{
        flexDirection:'row',
        justifyContent:'flex-start',
        alignItems:'center',
        borderBottomWidth:0.3,
        height:sizeHeight(7),
        backgroundColor:'white',
    },
    textItem:{
        textAlign:'center',
        fontSize:sizeFont(4),
        color:'black',
        marginTop:sizeHeight(0.3)
    },
    imageItem: {
        width:sizeWidth(38),
        height:sizeHeight(39),
        borderRadius:5
    },
    viewFlatList:{
        marginTop:sizeHeight(2),
        marginLeft:sizeWidth(4),
        marginRight:sizeWidth(4)
    }

});