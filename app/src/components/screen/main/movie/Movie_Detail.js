import React,{Component} from 'react';
import {
    View,Text,TouchableOpacity,Image,ImageBackground,StyleSheet,FlatList,ScrollView,AsyncStorage,TextInput
} from 'react-native';
import {sizeWidth,sizeHeight,sizeFont} from "../../../../utils/Size";
import Toolbar from '../../../common/ToolBar';
import ViewMoreText from 'react-native-view-more-text';
import {connect} from 'react-redux';
import {navigateToPage, goBack} from "../../../../redux/router/NavigationAction";
import CommentItem from '../../comment/CommentItem';
import {BASE_PATH_BACK_DROP, BASE_PATH_POSTER} from "../../../../Constant";
class MovieItem extends Component {
    constructor(props){
        super(props);
        this.state = {
            numberOfLines:4,
            idTouch:0,
            commentText:''
        }
    }
    renderBackdrop = (item) => {
        const backdrop_path = item && item.backdrop_path;
        const imageUri = BASE_PATH_BACK_DROP + backdrop_path;
        return(
            <View style={styles.viewBackdrop}>
                <Image
                    style={styles.imageBackDrop}
                    resizeMode={'stretch'}
                    source={{uri:imageUri}}/>
            </View>
        )
    };
    renderTopMovie = (item) => {
        const poster_path = item && item.poster_path;
        const imageUri = BASE_PATH_POSTER + poster_path;
        return(
            <View>
                <View style={styles.viewTopMovie}>
                    {this.renderImagePreview(imageUri)}
                    {this.renderViewDetail(item)}
                </View>
                <View style={styles.viewBottomMovie}>
                    {this.renderDescription(item.overview)}
                </View>
            </View>
        )
    };
    renderDay = (day) => {
        return(
            <View style={styles.viewDay}>
                <Text style={styles.textDay}>
                    Release day: {day}
                </Text>
            </View>
        )
    };
    renderNameMovie = (title) => {
        return(
            <View>
                <Text style={styles.textName}>
                    {title}
                </Text>
            </View>
        )
    };
    renderRate = (rate,vote_count) => {
        return(
            <View style={styles.viewRate}>
                <View style={styles.rates}>
                    <Text style={styles.textRate}>
                        {rate}
                    </Text>
                </View>
                <Text style={{marginLeft:sizeWidth(2),fontSize:sizeFont(5),color:'black'}}>
                    (Vote count: {vote_count})
                </Text>
            </View>
        )
    };
    renderViewDetail = (item) => {
        const title = item.title;
        const release_date = item.release_date;
        const rate = item.vote_average;
        const vote_count = item.vote_count;
        return(
            <View style={styles.viewDetail}>
                {this.renderNameMovie(title)}
                {this.renderDay(release_date)}
                {this.renderRate(rate,vote_count)}
            </View>
        )
    };
    renderDescription = (overview) => {
        return(
            <ViewMoreText
                numberOfLines={4}>
                <Text style={styles.textDescription}>
                    Overview:{"\n"}
                    {overview}
                </Text>
            </ViewMoreText>
        )
    };
    renderImagePreview = (imagePath) => {
        return(
            <View style={styles.viewImage}>
                <Image
                    style={styles.imagePreview}
                    resizeMode={'cover'}
                    source={{uri:imagePath}}/>
            </View>
        )
    };
    renderNavigationBar = () => {
        return(
            <View style={styles.viewTitle}>
                <TouchableOpacity
                    onPress={() => {this.props.navigation.goBack()}}
                    style={styles.viewBackButton}>
                    <Image
                        style={styles.backButton}
                        source={require('../../../../../resources/icon/left-arrow.png')}/>
                </TouchableOpacity>
                <Text style={styles.textTitle}>
                    Detail
                </Text>
            </View>
        )
    };
    renderLike = (item) => {
        return(
            <View style={styles.viewLike}>
                <View style={{flexDirection:'row'}}>
                    <TouchableOpacity
                        onPress={()=>{this.pressLike(item)}}
                        style={styles.touchLike}>
                        <Image
                            style={styles.imageLike}
                            source={require('../../../../../resources/icon/like.png')}/>
                    </TouchableOpacity>
                </View>
                <View style={{flexDirection:'row'}}>
                    <TouchableOpacity
                        onPress={()=>{this.pressComment(item)}}
                        style={styles.touchLike}>
                        <Image
                            style={styles.imageLike}
                            source={require('../../../../../resources/icon/comment.png')}/>
                    </TouchableOpacity>
                </View>
            </View>
        )
    };
    pressComment = (item) => {
        const information = item;
        this.props.navigateToPage('Comment',{information});
    };
    pressLike = (item) => {
        this.setLike(item)
    };
    setLike = async(item) => {
        try {
            AsyncStorage.getItem("@ID" + item.id + ":key", (err, result) => {
                console.log("ABC" + result);
                if (result == null) {
                    AsyncStorage.setItem("@ID" + item.id + ":key", JSON.stringify(item));
                    AsyncStorage.getItem("@TOTALLIKE:key", (err, result) => {
                        console.log("ABC" + result);
                        const TotalItem = JSON.parse(result);
                        console.log("ABCDEGH" + TotalItem);
                        TotalItem.push(item);
                        AsyncStorage.setItem("@TOTALLIKE:key",JSON.stringify(TotalItem));
                    });
                    const CountString = AsyncStorage.getItem("@NUMBERCOUNT:key",(err,result) => {
                        const count = JSON.parse(result);
                        const Like = {
                            like: count.like + 1
                        };
                        AsyncStorage.mergeItem("@NUMBERCOUNT:key",JSON.stringify(Like));
                        console.log("NUMBER COUNT" + JSON.stringify(AsyncStorage.getItem("@NUMBERCOUNT:key")))
                    });

                }
            });
        }
        catch(e){
            console.log(e)
        }
    };
    countWatch = async() => {
        try {
                AsyncStorage.getItem("@NUMBERCOUNT:key",(err,result) => {
                const count = JSON.parse(result);
                const Watch = {
                    watch: count.watch + 1
                };
                AsyncStorage.mergeItem("@NUMBERCOUNT:key",JSON.stringify(Watch));
                console.log("NUMBER COUNT" + JSON.stringify(AsyncStorage.getItem("@NUMBERCOUNT:key")))
            });
        }
        catch(e){
            console.log(e)
        }
    };
    render(){
        const { params } = this.props.navigation.state;
        const item = params ? params.information : null;
        this.countWatch(item);
        return(
            <View style={{flex:1}}>
                <Toolbar/>
                <View>
                    {this.renderNavigationBar()}
                </View>
                <ScrollView style={{flex:1}}>
                    {this.renderBackdrop(item)}
                    {this.renderTopMovie(item)}
                    {this.renderLike(item)}
                </ScrollView>
            </View>
        )
    }
}
export default connect(null,{navigateToPage})(MovieItem);
const styles = StyleSheet.create({
    viewTopMovie:{
        flexDirection:'row',
        marginLeft:sizeWidth(2),
        marginRight:sizeWidth(2),
        backgroundColor:'white',
    },
    viewBottomMovie:{
        marginLeft:sizeWidth(2),
        marginRight:sizeWidth(2),
        backgroundColor:'white',
        alignItems:'center',
        marginBottom:sizeHeight(1),
        padding:sizeWidth(2)
    },
    viewLike:{
        flexDirection:'row',
        marginLeft:sizeWidth(2),
        marginRight:sizeWidth(2),
        backgroundColor:'white',
        justifyContent:'space-around',
        alignItems:'center',
        marginBottom:sizeHeight(1),
        padding:sizeWidth(1)
    },
    touchLike:{
        borderRadius:60,
        borderWidth:1,
        alignItems:'center',
        justifyContent:'center',
        padding:sizeWidth(1),
    },
    imageLike:{
        width:sizeWidth(6),
        height:sizeWidth(6)
    },
    textLike:{
        fontSize:sizeWidth(4),
        marginTop:sizeWidth(5),
        marginLeft:sizeWidth(1),
        color:'skyblue'
    },
    viewImage:{
        width:sizeWidth(27),
        height:sizeHeight(25),
        marginLeft:sizeWidth(2),
        marginTop:sizeHeight(0.3),
        alignItems:'center',
    },
    imagePreview:{
        width:sizeWidth(27),
        height:sizeHeight(24),
        borderRadius:5
    },
    viewBackdrop:{
        height:sizeHeight(33)
    },
    textName:{
        fontSize:sizeFont(8),
        color:'black',
    },
    viewDay:{
        marginTop:sizeWidth(1)
    },
    textDay:{
        fontSize:sizeFont(5),
        color:'black'
    },
    textRate:{
        color:'white',
        fontSize:sizeFont(6)
    },
    rates:{
        width:sizeWidth(9),
        height:sizeWidth(9),
        backgroundColor:'black',
        justifyContent:'center',
        alignItems:'center',
        borderRadius:50,
    },
    viewDescription:{
        marginLeft:sizeWidth(2),
        marginRight:sizeWidth(2),
        backgroundColor:'white'
    },
    viewRate:{
        marginTop:sizeWidth(1),
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center'
    },
    textDescription:{
        fontSize:sizeFont(5),
        color:'black'
    },
    viewDetail:{
        marginLeft:sizeWidth(3),
        width:sizeWidth(60),
        alignItems:'flex-start',
    },
    viewInformation:{
        height:sizeHeight(70)
    },
    imageBackDrop:{
        height:sizeHeight(33),
        width:sizeWidth(100)
    },
    viewTitle:{
        flexDirection:'row',
        justifyContent:'flex-start',
        alignItems:'center',
        borderBottomWidth:0.3,
        height:sizeHeight(7),
        backgroundColor:'white'
    },
    viewBackButton:{
        alignItems:'center',
        justifyContent:'center',
        marginLeft:sizeWidth(6),
        width:sizeWidth(8),
        height:sizeWidth(6)
    },
    backButton:{
        width:sizeWidth(6),
        height:sizeWidth(5),
    },
    textTitle:{
        marginLeft:sizeWidth(6),
        fontSize:sizeFont(6),
        color:'black'
    },
});