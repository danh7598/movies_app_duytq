import React,{Component} from 'react';
import {
    View,Text,TouchableOpacity,Image,StyleSheet,FlatList,ActivityIndicator
} from 'react-native';
import {sizeWidth,sizeHeight,sizeFont} from "../../../../utils/Size";
import {connect} from 'react-redux';
import {navigateToPage, goBack} from "../../../../redux/router/NavigationAction";
import Toolbar from '../../../common/ToolBar';
import {actionGetMovieList,actionRefreshMovieList} from "../../../../redux/movie/MovieAction";
import PopularItemMovie from './PopularItemMovie';
class Movie extends Component {
    constructor(props){
        super(props);
        this.state = {
            loading: this.props.showLoading,
            isRefreshing:false
        }
    }
    componentDidMount() {
        this.props.actionGetMovieList(1);
    }
    _keyExtractor = (item) => item.id.toString();
    renderMovieTitle = () => {
        return(
            <View>
                <View style={styles.movieTitle}>
                    <Text style={styles.textMovieTitle}>
                        MOVIE
                    </Text>
                    <TouchableOpacity
                        onPress={()=>{this.goToSearch()}}
                        style={styles.searchView}>
                        <Image
                            style={styles.searchButton}
                            source={require('../../../../../resources/icon/search.png')}/>
                    </TouchableOpacity>
                </View>
            </View>
        )
    };
    goToSearch = () => {
        const type = 'Movie';
        this.props.navigateToPage('Search',{type})
    };
    renderLoading = () => {
        const loading = this.props.showLoading;
        if (loading) {
            return (
                <ActivityIndicator
                    size={'large'}
                    color={'black'}
                />
            )
        }
    };
    onRefresh() {
        this.setState({
            isRefreshing: true,
        }, function() {
            this.afterPull();
        });
    }
    afterPull = () => {
        this.props.actionRefreshMovieList();
        this.props.actionGetMovieList(1);
        this.setState({
            isRefreshing: false,
        });
    };
    renderPopularList = () => {
        return(
            <View>
                <View style={styles.popularMovieTitle}>
                    <Text style={styles.textPopularTitle}>
                        Upcoming Movie
                    </Text>
                    <TouchableOpacity
                        onPress={() => {this.props.navigateToPage('PopularMovieList')}}
                        style={styles.buttonMORE}>
                        <Text style={styles.textButtonMORE}>
                            MORE
                        </Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.ListPopular}>
                    {this.renderLoading()}
                    <FlatList
                        onRefresh={() => this.onRefresh()}
                        refreshing={this.state.isRefreshing}
                        keyExtractor={this._keyExtractor}
                        horizontal={false}
                        numColumns={2}
                        showsVerticalScrollIndicator={false}
                        data={this.props.upComingMovies}
                        renderItem={({item,index}) => {
                            return(
                                <PopularItemMovie item={item} index={index}>
                                </PopularItemMovie>
                            )
                        }}>
                    </FlatList>
                </View>
            </View>
        )
    };
    render(){
        return(
            <View style={{flex:1}}>
                <Toolbar/>
                <View style={{flex:1}}>
                    {this.renderMovieTitle()}
                    {this.renderPopularList()}
                </View>
            </View>
        )
    }
}

export default connect(state => ({
    nav: state.nav,
    showLoading: state.movieState.showLoading,
    upComingMovies: state.movieState.upComingMovies
}), {navigateToPage, goBack, actionGetMovieList, actionRefreshMovieList})(Movie);

const styles = StyleSheet.create({
    movieTitle:{
        flexDirection:'row',
        borderBottomWidth:0.3,
        justifyContent:'space-between',
        alignItems:'center',
        height:sizeHeight(8),
        backgroundColor:'white'
    },
    searchView:{
        alignItems:'flex-end',
        justifyContent:'center',
        marginRight:sizeWidth(6)
    },
    searchButton:{
        width:sizeWidth(7),
        height:sizeWidth(7)
    },
    textMovieTitle:{
        fontSize:30,
        marginLeft:sizeWidth(6),
        color:'black',
    },
    popularMovieTitle:{
        backgroundColor:'white',
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',
        marginTop:sizeHeight(2),
        borderRadius:5,
        margin:sizeWidth(2),
        height:sizeHeight(6)
    },
    textPopularTitle:{
        marginLeft:sizeWidth(4),
        fontSize:25,
        color:'black'
    },
    buttonMORE:{
        marginRight:sizeWidth(4),
        borderRadius:5,
    },
    textButtonMORE:{
        fontSize:sizeFont(4),
        fontWeight:'bold',
        color:'black',
        padding:3
    },
    imageMovieItem:{
        marginTop:sizeHeight(3),
        width:sizeWidth(70),
        height:sizeHeight(20),
        borderRadius:5
    },
    FlatListMovie:{
        flexDirection:'row',
        marginTop:sizeHeight(1),
        marginLeft:sizeWidth(6),
    },
    ListPopular:{
        backgroundColor:'transparent',
        marginTop:sizeHeight(0.5),
        marginRight:sizeWidth(2),
        marginBottom:sizeHeight(26.5)
    },
}) ;