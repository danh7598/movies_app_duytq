import React,{Component} from 'react';
import {
    View,Text,TouchableOpacity,Image,ImageBackground,StyleSheet,FlatList,ScrollView,Dimensions
} from 'react-native';
import {sizeWidth,sizeHeight,sizeFont} from "../../../../utils/Size";
import {connect} from 'react-redux';
import {navigateToPage, goBack} from "../../../../redux/router/NavigationAction";
import {BASE_PATH_POSTER} from "../../../../Constant";
class PopularItemMovie extends Component{
    render(){
        const movie = this.props.item;
        const title = movie && movie.title;
        const poster_path = movie && movie.poster_path;
        const imageUri = BASE_PATH_POSTER + poster_path;
        return(
            <TouchableOpacity
                onPress={()=>{this.goToMovieItem()}}
                style={styles.MovieItem}>
                <Image
                    style={styles.imagePopularItem}
                    source={{uri:imageUri}}
                    resizeMode={'cover'}/>
                <View style={styles.viewTextMovieItem}>
                    <Text
                        numberOfLines={1}
                        style={styles.textMovieItem}>{title}</Text>
                </View>
            </TouchableOpacity>
        )
    }
    goToMovieItem = () => {
        const information = this.props.item;
        this.props.navigateToPage('MovieItem',{information});
    }
}

export default connect(null,{navigateToPage})(PopularItemMovie);
const styles = StyleSheet.create({
    MovieItem:{
        justifyContent:'center',
        alignItems:'center',
        borderRadius:5,
        marginLeft:sizeWidth(2),
        marginBottom:sizeHeight(1),
        backgroundColor:'white'
    },
    imagePopularItem:{
        marginLeft:sizeWidth(2),
        marginRight:sizeWidth(2),
        marginTop:sizeWidth(2),
        width:sizeWidth(43),
        height:sizeHeight(35),
        borderRadius:5,
    },
    viewTextMovieItem:{
        justifyContent:'center',
        alignItems:'center',
        width:sizeWidth(35)
    },
    textMovieItem:{
        fontSize:sizeWidth(4.5),
        color:'black',
        marginTop:sizeHeight(0.3),
        textAlign:'center'
    },
});