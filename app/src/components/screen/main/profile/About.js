import React,{Component} from 'react';
import {
    View, Text, TouchableOpacity, Image, ImageBackground, StyleSheet, FlatList, ScrollView, AsyncStorage,
    RefreshControl
} from 'react-native';
import {sizeWidth,sizeHeight,sizeFont} from "../../../../utils/Size";
import {navigateToPage, goBack} from "../../../../redux/router/NavigationAction";
import Toolbar from '../../../common/ToolBar';
import {connect} from "react-redux";
import {BASE_PATH_POSTER} from "../../../../Constant";
class About extends Component{
    renderAbout = () => {
        return(
            <View style={styles.container}>
                <View style={{marginTop:sizeHeight(20)}}>
                    <Text style={styles.textTop}>
                        About:
                    </Text>
                    <Text style={styles.textBottom}>
                        First app design
                    </Text>
                    <Text style={styles.textBottom}>
                        Release: 27-4-2018
                    </Text>
                </View>
            </View>
        )
    };
    renderNavigationBar = () => {
        return(
            <View style={styles.viewTitle}>
                <TouchableOpacity
                    onPress={() => {this.props.navigation.goBack()}}
                    style={styles.viewBackButton}>
                    <Image
                        style={styles.backButton}
                        source={require('../../../../../resources/icon/left-arrow.png')}/>
                </TouchableOpacity>
                <Text style={styles.textTitle}>
                    About
                </Text>
            </View>
        )
    };
    render(){
        return(
            <View>
                <Toolbar/>
                {this.renderNavigationBar()}
                {this.renderAbout()}
            </View>
        )
    }
}
export default connect(null,{navigateToPage,goBack})(About)
const styles = StyleSheet.create({
    container:{
        alignItems:'center',
        backgroundColor:'white',
        height:sizeHeight(89.7)
    },
    textTop:{
        fontSize:sizeFont(8),
        color:'black',
        textAlign:'center'
    },
    textBottom:{
        fontSize:sizeFont(6),
        color:'black',
        textAlign:'center'
    },
    viewTitle:{
        flexDirection:'row',
        justifyContent:'flex-start',
        alignItems:'center',
        borderBottomWidth:0.3,
        height:sizeHeight(7),
        backgroundColor:'white'
    },
    viewBackButton:{
        alignItems:'center',
        justifyContent:'center',
        marginLeft:sizeWidth(6),
        width:sizeWidth(8),
        height:sizeWidth(6)
    },
    backButton:{
        width:sizeWidth(6),
        height:sizeWidth(5),
    },
    textTitle:{
        marginLeft:sizeWidth(6),
        fontSize:sizeFont(6),
        color:'black'
    },
});