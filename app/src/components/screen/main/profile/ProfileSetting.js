import React,{Component} from 'react';
import {
    View,Text,TouchableOpacity,Image,ImageBackground,StyleSheet,FlatList,ScrollView
} from 'react-native';
import {sizeWidth,sizeHeight,sizeFont} from "../../../../utils/Size";
import {navigateToPage, goBack} from "../../../../redux/router/NavigationAction";
import Toolbar from '../../../common/ToolBar';
import {connect} from "react-redux";
class ProfileSetting extends Component{
    renderRow = (text) => {
        return(
            <TouchableOpacity
                style={styles.row}>
                <View style={{width:sizeWidth(84)}}>
                    <Text style={styles.textSetting}>
                        {text}
                    </Text>
                </View>
                <Image
                    source={require('../../../../../resources/icon/next.png')}
                    style={styles.nextImage}/>
            </TouchableOpacity>
        )
    };
    renderTitle = () => {
        return(
            <View style={styles.viewTitle}>
                <TouchableOpacity
                    onPress={() => {this.props.navigation.goBack()}}
                    style={styles.viewBackButton}>
                    <Image
                        style={styles.backButton}
                        source={require('../../../../../resources/icon/left-arrow.png')}/>
                </TouchableOpacity>
                <Text style={styles.title}>
                    Setting
                </Text>
            </View>
        )
    };
    render(){
        return(
            <View>
                <Toolbar/>
                {this.renderTitle()}
                {this.renderRow('Clear cache')}
                {this.renderRow('Share the app')}
                {this.renderRow('Question')}
                {this.renderRow('About')}
            </View>
        )
    }
}
export default connect(null,{navigateToPage})(ProfileSetting);
const styles = StyleSheet.create({
    nextImage:{
        marginLeft:sizeWidth(6),
        height:sizeWidth(6),
        width:sizeWidth(6),
        marginRight:sizeWidth(6),
        tintColor:'grey'
    },
    row:{
        flexDirection:'row',
        alignItems:'center',
        height:sizeHeight(8),
        borderBottomWidth:1,
        borderColor:'grey'
    },
    textSetting:{
        marginLeft:sizeWidth(5),
        fontSize:sizeFont(4),
        color:'black'
    },
    viewTitle:{
        flexDirection:'row',
        justifyContent:'flex-start',
        alignItems:'center',
        borderBottomWidth:0.4,
        height:sizeHeight(7)
    },
    viewBackButton:{
        alignItems:'center',
        justifyContent:'center',
        marginLeft:sizeWidth(6),
        width:sizeWidth(8),
        height:sizeWidth(6)
    },
    backButton:{
        width:sizeWidth(6),
        height:sizeWidth(5),
    },
    title:{
        marginLeft:sizeWidth(6),
        fontSize:sizeFont(6),
        color:'black'
    },
});