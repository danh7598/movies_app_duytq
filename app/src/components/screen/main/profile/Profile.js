import React,{Component} from 'react';
import {
    View, Text, TouchableOpacity, Image, ImageBackground, StyleSheet, FlatList, ScrollView, AsyncStorage,
    RefreshControl, BackHandler
} from 'react-native';
import {sizeWidth,sizeHeight,sizeFont} from "../../../../utils/Size";
import {navigateToPage, goBack} from "../../../../redux/router/NavigationAction";
import Toolbar from '../../../common/ToolBar';
import {connect} from "react-redux";
import {BASE_PATH_POSTER} from "../../../../Constant";
import About from './About';
class ListItem extends Component{
    render(){
        const item = this.props.item;
        const poster_path = item && item.poster_path;
        const imageUri = BASE_PATH_POSTER + poster_path;
        return(
            <View>
                <Image
                    source={{uri:imageUri}}
                    style={styles.listImage}/>
            </View>
        )
    }
}
class Profile extends Component{
    constructor(props){
        super(props);
        this.state = {
            isRefresh:false,
            dataItem:[],
            dataCount:{},
        }
    }
    componentWillUnmount(){
        BackHandler.exitApp()
    }

    renderTop = () => {
        return(
            <View style={styles.top}>
                <View style={styles.topTitle}>
                    <Text style={styles.textProfileTitle}>
                        Profile
                    </Text>
                    <TouchableOpacity
                        onPress={() => {this.props.navigateToPage('About')}}
                        style={styles.settingView}>
                        <Image
                            style={styles.settingButton}
                            source={require('../../../../../resources/icon/settings.png')}/>
                    </TouchableOpacity>
                </View>
                <View style={styles.viewUser}>
                    <View style={styles.viewImageUser}>
                        <Image
                            style={styles.imageUser}
                            source={require('../../../../../resources/icon/user.png')}/>
                    </View>
                </View>
                <View style={styles.topName}>
                    <Text style={styles.textName}>
                        Tạ Quang Duy
                    </Text>
                </View>
            </View>
        )
    };
    renderBottom = () => {
        const like = this.state.dataCount.like;
        const watch = this.state.dataCount.watch;
        const comment = this.state.dataCount.comment;
        return(
            <View style={styles.bottom}>
                <View style={styles.viewStatistic}>
                    <View style={styles.component}>
                        <Text style={styles.numberComponent}>
                            {like}
                        </Text>
                        <Text style={styles.nameComponent}>
                            Like
                        </Text>
                    </View>
                    <View style={styles.component}>
                        <Text style={styles.numberComponent}>
                            {watch}
                        </Text>
                        <Text style={styles.nameComponent}>
                            Watching
                        </Text>
                    </View>
                    <View style={styles.component}>
                        <Text style={styles.numberComponent}>
                            {comment}
                        </Text>
                        <Text style={styles.nameComponent}>
                            Comments
                        </Text>
                    </View>
                </View>
                <View style={styles.listMovie}>
                    <FlatList
                        refreshing={this.state.isRefresh}
                        onRefresh={() => {this.onRefresh()}}
                        keyExtractor={this._keyExtractor}
                        numColumns={4}
                        showsHorizontalScrollIndicator={false}
                        data={this.state.dataItem}
                        renderItem={({item,index}) => {
                            return(
                                <ListItem item={item} index={index}>
                                </ListItem>
                            )
                        }}/>
                </View>
            </View>
        )
    };
    get = async() => {
        try{
            const TotalString = await AsyncStorage.getItem("@TOTALLIKE:key");
            TotalItem = JSON.parse(TotalString);
            console.log('PROFILE GET ITEM LIKE' + TotalItem);
            this.setState({
                dataItem:TotalItem
            });
            const CountString = await AsyncStorage.getItem("@NUMBERCOUNT:key");
            TotalCount = JSON.parse(CountString);
            console.log('PROFILE GET ITEM COUNT' + TotalCount);
            this.setState({
                dataCount:TotalCount
            });
        }catch(e){
            console.log(e)
        }
    };
    componentDidMount(){
        this.get();
    }
    _keyExtractor = (item, index) => item.id.toString();
    onRefresh() {
        this.setState({
            isRefresh: true,
        }, function() {
            this.get();
            this.setState({
                isRefresh: false,
            });
        });
    }
    render(){
        return(
            <View style={{flex:1}}>
                <Toolbar/>
                {this.renderTop()}
                <View>
                    {this.renderBottom()}
                </View>
            </View>
        )
    }
}
let TotalItem = [];
let TotalCount = {};
const styles = StyleSheet.create({
    bottom:{
        height:sizeHeight(60)
    },
    top:{
        height:sizeHeight(36.7),
        backgroundColor:'white',
        borderBottomWidth:0.3
    },
    topTitle:{
        flexDirection:'row',
        backgroundColor:'white',
        borderBottomWidth:0.3,
    },
    settingButton:{
        width:sizeWidth(7),
        height:sizeWidth(7),
        tintColor:'black'
    },
    settingView:{
        marginTop:sizeHeight(1),
        alignItems:'flex-end',
        justifyContent:'center',
        width:sizeWidth(60)
    },
    textProfileTitle:{
        fontSize:sizeFont(10),
        marginTop:sizeHeight(1),
        marginLeft:sizeWidth(6),
        color:'black'
    },
    viewUser:{
        height:sizeHeight(21),
        alignItems:'center',
        justifyContent:'center'
    },
    viewImageUser:{
        borderRadius:70,
        borderWidth:1,
        padding:sizeWidth(4)
    },
    imageUser:{
        height:sizeWidth(20),
        width:sizeWidth(20),
    },
    topName:{
        height:sizeHeight(7),
        marginRight:sizeWidth(10),
        marginLeft:sizeWidth(10),
        alignItems:'center',
        justifyContent:'center',
    },
    textName:{
        fontSize:sizeFont(5),
        color:'black'
    },
    viewStatistic:{
        height:sizeHeight(10),
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center'
    },
    component:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
    },
    numberComponent:{
        fontSize:sizeFont(5),
        color:'black',
        fontWeight:'bold'
    },
    nameComponent:{
        fontSize:sizeFont(4),
        color:'grey'
    },
    listMovie:{
        marginLeft:sizeWidth(6),
        marginRight:sizeWidth(3),
        marginBottom:sizeHeight(18)
    },
    listImage:{
        height:sizeHeight(17),
        width:sizeWidth(19.75),
        marginRight:sizeWidth(3),
        borderRadius:10,
        marginBottom:sizeWidth(3)
    }

});
export default connect(null,{navigateToPage})(Profile);