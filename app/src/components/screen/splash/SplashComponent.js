import React, {Component} from 'react';
import {AsyncStorage } from 'react-native';
import {resetPage} from "../../../redux/router/NavigationAction";
import {connect} from "react-redux";
import SplashScreen from 'react-native-splash-screen'
import {
    View,
    StyleSheet,
} from "react-native";

class SplashComponent extends Component {
    constructor(props){
        super(props);
        this.state = {
            mainPage: 'WalkThrough'
        }

    }
    componentWillMount(){
        this.get()
    }
    get = async() => {
        try{
            const page = await AsyncStorage.getItem("@PAGEMAIN:key");
            console.log('GET OK');
            if(page == 'Did WalkThrough'){
                this.setState({
                    mainPage:'Login'
                })
            }
        }catch(e){
            console.log(e)
        }
    };
    handleNavigatorWelcome = () => {
        SplashScreen.hide();
        const {resetPage} = this.props;
        resetPage(this.state.mainPage)
    };

    render() {
        return (
            <View>
            </View>
        );
    }

    componentDidMount() {
        setTimeout(this.handleNavigatorWelcome, 750);
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ccc',
        justifyContent: 'center',
        alignItems: 'center'
    }
});


export default connect( null, {
    resetPage
})(SplashComponent);