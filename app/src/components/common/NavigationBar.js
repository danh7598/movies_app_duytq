import React,{Component} from 'react';
import {
    View,Text,TouchableOpacity,Image,ImageBackground,StyleSheet,FlatList,ScrollView
} from 'react-native';
import {sizeWidth,sizeHeight,sizeFont} from "../../utils/Size";
import {navigateToPage, goBack} from "../../redux/router/NavigationAction";
import Toolbar from './ToolBar';
import {connect} from "react-redux";
export default class NavigationBar extends Component{
    render(){
        return(
            <View style={styles.viewTitle}>
                <TouchableOpacity
                    onPress={() => {this.props.navigation.goBack()}}
                    style={styles.viewBackButton}>
                    <Image
                        style={styles.backButton}
                        source={require('../../../../../resources/icon/left-arrow.png')}/>
                </TouchableOpacity>
                <Text style={styles.title}>
                </Text>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    viewTitle:{
        flexDirection:'row',
        justifyContent:'flex-start',
        alignItems:'center',
        borderBottomWidth:1
    },
    viewBackButton:{
        alignItems:'center',
        justifyContent:'center',
        marginLeft:sizeWidth(6),
        marginTop:sizeHeight(1),
        width:sizeWidth(8),
        height:sizeWidth(6)
    },
    backButton:{
        width:sizeWidth(6),
        height:sizeWidth(5),
    },
    title:{
        marginLeft:sizeWidth(6),
        fontSize:sizeFont(7),
        marginTop:sizeHeight(1),
        color:'black'
    },
});
