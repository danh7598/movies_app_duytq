import React, {Component} from 'react';
import AppWithNavigationState from "../../redux/router/AppRouter";
import {
    StatusBar,
    View
} from 'react-native';

export default class AppMain extends Component {
    render() {
        return (
            <View style={{flex: 1}}>
                <StatusBar
                    backgroundColor={'transparent'}
                    barStyle="light-content"
                    translucent={true}
                />

                <AppWithNavigationState/>
            </View>
        );
    }
}
