package com.movie_app_duytq;

import com.facebook.react.ReactActivity;
import org.devio.rn.splashscreen.SplashScreen;
import com.facebook.react.bridge.ReactContext;
import android.os.Bundle;

public class MainActivity extends ReactActivity {

    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "movie_app_duytq";
    }
    @Override
            protected void onCreate(Bundle savedInstanceState) {
                ReactContext context = getReactInstanceManager().getCurrentReactContext();

                if(context == null) {
                    SplashScreen.show(this);
                }

                super.onCreate(savedInstanceState);
            }

}
